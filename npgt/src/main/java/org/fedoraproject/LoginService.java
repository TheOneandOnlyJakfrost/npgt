/**
 *
 * @author Stephen Snow
 */
package org.fedoraproject;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LoginService {

    public String login(String name) {
        return "hello " + name;
    }

}
