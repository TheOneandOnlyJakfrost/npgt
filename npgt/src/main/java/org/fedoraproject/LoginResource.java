package org.fedoraproject;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
/**
 *
 * @author Stephen Snow
 */

@Path("/login")
public class LoginResource {
    @Inject
    LoginService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{name}")
    public String login(@PathParam String name) {
        return service.login(name);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String login() {
        return "Your FAS Id";
    }
    
}
