package org.fedoraproject;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author Stephen Snow
 */

@QuarkusTest
public class LoginResourceTest {
    @Test
    public void testLoginEndpoint() {
        given()
          .when().get("/login")
          .then()
             .statusCode(200)
             .body(is("Your FAS Id"));
    }
    
    @Test
    public void testLoginNameEndpoint() {
        String uuid = UUID.randomUUID().toString();
        given()
          .pathParam("name", uuid)
          .when().get("/{name}")
          .then()
            .statusCode(200)
            .body(is("hello " + uuid));
    }
    
}
