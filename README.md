# NPGT

New Packager Guide Tool

## Getting started

To make it easy for you to get started with packaging contributions for Fedora Linux, 
here's a cloud based application that guides the new packager through the steps
of creating a Fedora Linux RPM, then package. (Details on methodology to be fleshed out)


## Integrate with your tools 
(Need to decide what is desired)

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://gitlab.com/TheOneandOnlyJakfrost/npgt/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

(This should actually be whatever Fedora uses)

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:685a7ce42c4a8b9bc94028431e677c2f?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***



## Name
NPGT - New Packager Guide Tool.

## Description
This application is an effort to establish a standard approach to quantify and qualify prospective New Packagers for the Fedora Linux Package Maintenance Group.
It runs in a container and can run either as an OpenJdk containerized application or as a native containerized application. It may be browsed at localhost:8080 with any modern web browser, it is Restful.
The maintenance of packages for Fedora Linux releases, requires specific knowledge/skills that can be roughly divided into four areas, RPM Packaging for Fedora Linux, Fedora Linux Release cycle and how it relates to Package Maintenance, the Fedora Project Build System, and the Fedora Package Review system. 
The goal of this application is to introduce the prospective packager(s) to the Fedora Packaging format using the Fedora Packaging Tutorial already in existence. The user would create a COPR repo to use as the packaging tutorial repo. After completing the tutorial, it can be reviewed  the current Package Sponsors/Mentors to evaluate capabilities and understanding of the Package Maintainers responsibilities to the Fedora Community and Fedora Project for the realease of Fedora Linux Distribution.

## Badges
Badges are intended to be created for this application, and if they are it would be an easy visual indicator of a users packaging capability.

## Installation
Please refer to the README.md in the project directory for complete details on running this in development mode as well as compiling an using.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Please submit issues here as I will be monitoring this closely. Thank you for taking the time to do so.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
All contributors are welcome! Fork it, clone it, fix then submit a PR. I'll try to be responsive, I promise!


## Authors and acknowledgment
npgt Was created by Stephen Snow after discussion with Otto Urpelainen and Vit Ondruch, and basically cherry picking their good ideas then massaging them into this blob we can work on.

## License
This project is licensed under the GNU General Public License v3.0. Learn more

## Project status
The goal of this project could be to become an available Package Maintainer entry tool for the Fedora Project. An application that exposes current knowledge and systems to prospective package maintainers, in an opinionated way that reflects the Fedora Project community and the Fedora Linux Release requirements on Package Maintainers in particular. The desired result is to provide the prospective Package Maintainers with the skill sets needed to be successful, and also to provide the Package Maintenance group a method of guaging readiness of prospective packagers to take on packaging duties. This all depends on the community, and whether this app is determined by them to be useful. But I think the most important goal is to get the community involved in shaping this project to suit our needs.

